//
//  PokedexTests.swift
//  PokedexTests
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import XCTest
@testable import Pokedex


class WorkerMock: PokedexWorkerProtocol {
    
    var testBool = false
    var serviceMock: ServiceMock!
    
    func getPokemonList(pageOfset: Int, reponseObject: @escaping (Result<PokedexModel.Response, NetworkError>) -> Void) {
        testBool = true
        serviceMock.getPokemonList(pageOffset: pageOfset) {
            result in
            reponseObject(.failure(NetworkError.generalError))
        }
    }
}

class ServiceMock: PokedexServiceProtocol {
    
    var testBool = false
    
    func getPokemonList(pageOffset: Int, result: @escaping (Result<Data, NetworkError>) -> Void) {
        testBool = true
        result(.failure(NetworkError.generalError))
    }
}

class PokedexTests: XCTestCase {
    
    var interactorSut: PokedexInteractor!
    var mockWorker: WorkerMock!
    var serviceMock: ServiceMock!
    var pokemonDetailServiceSut: PokemonDetailService!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        mockWorker = WorkerMock()
        serviceMock = ServiceMock()
        mockWorker.serviceMock = serviceMock
        interactorSut = PokedexInteractor(mockWorker)
        pokemonDetailServiceSut = PokemonDetailService()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        interactorSut = nil
        mockWorker = nil
        serviceMock = nil
        pokemonDetailServiceSut = nil
    }
    
    func testInteractor() {
        XCTAssertFalse(mockWorker.testBool)
        XCTAssertFalse(serviceMock.testBool)
        interactorSut.getPokemonList(0)
        XCTAssertTrue(mockWorker.testBool)
        XCTAssertTrue(serviceMock.testBool)
    }
    
    func testPokemonId() {
        XCTAssertEqual(pokemonDetailServiceSut.getPokemonId(urlString: "urlPath/12/"), 12)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
