//
//  PokedexUITests.swift
//  PokedexUITests
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//


import XCTest

class PokedexUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        app = nil
    }

    func testPokedexFlow() {
        XCTAssertEqual(app.otherElements.staticTexts["header"].label, "Pokedex")
        let cell = app.tables.cells["cell_0"]
        XCTAssertTrue(cell.waitForExistence(timeout: 10))
        cell.tap()
        XCTAssertTrue(app.otherElements.staticTexts["pokemonName"].waitForExistence(timeout: 10))
        XCTAssertNotEqual(app.otherElements.staticTexts["pokemonName"].label, "")
    }
}
