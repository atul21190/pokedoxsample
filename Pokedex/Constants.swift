//
//  File.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation

let END_POINT = "/api/v2/pokemon"
let HOST = "https://pokeapi.co"
let PAGE_LIMIT = "20"
