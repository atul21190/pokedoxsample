//
//  APIManager.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation

enum HttpMethod: String {
    case GET
    case POST
    case PUT
    case DELETE
}

enum NetworkError: Error {
    case badRequest
    case generalError
}

class APIManager {
    
    public static func requestData(method: HttpMethod, urlQuery: [String: String], endPoint: String, response: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let urlSession = URLSession.shared
        var urlComponent = URLComponents(string: HOST + endPoint)
        urlComponent?.queryItems = [URLQueryItem]()
        for item in urlQuery {
            let urlQuery = URLQueryItem(name: item.key, value: item.value)
            urlComponent?.queryItems?.append(urlQuery)
        }
        guard let url = urlComponent?.url else {
            return
        }
        let urlRequest = URLRequest(url: url)
        urlSession.dataTask(with:urlRequest, completionHandler: {
            (data, urlresponse, error) in
            response(data, urlresponse, error)
        }).resume()
    }
    
    public static func downloadImage(urlString: String, response: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let urlSession = URLSession.shared
        guard let url = URL(string: urlString) else {
            return
        }
        urlSession.dataTask(with: url) {
            (data, urlresponse, error) in
            response(data, urlresponse, error)}.resume()
    }
}
