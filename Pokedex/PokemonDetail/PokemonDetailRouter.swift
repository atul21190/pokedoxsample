//
//  PokemonDetailRouter.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation
import UIKit

class PokemonDetailRouter {
    
    func dismissController(controller: UIViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
