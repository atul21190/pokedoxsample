//
//  PokemonDetailViewController.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation
import UIKit

protocol PokemonDetailViewControllerProtocol {
    func displayResponse(response: PokemonDetailModel.Response)
    func displayImageData(response: Data)
    func stopAnimation()
}

class PokemonDetailViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var tapGestureView: UIView!
    @IBOutlet weak var pokemonImage: UIImageView!
    @IBOutlet weak var pokemonName: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var detailViewHeight: NSLayoutConstraint!
    
    
    var router: PokemonDetailRouter?
    var interactor: PokemonDetailInteractor?
    var pokemon: PokemonDetailModel.Response?
    var urlString: String?
    let detailHeight:CGFloat = 300
    let timeDurationForAnimation = 0.5
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        PokemonDetailConfigurator.configureVIP(viewController: self)
        setUpView()
        interactor?.getPokemonDetail(urlString: urlString ?? "")
    }
    
    func setUpView() {
        self.activityIndicator.startAnimating()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureCallBack))
        tapGestureView.addGestureRecognizer(tapGesture)
        pokemonName.accessibilityIdentifier = "pokemonName"
        self.view.layoutIfNeeded()
        detailView.layer.cornerRadius = 20.0
        detailView.layer.masksToBounds = true
        detailView.clipsToBounds = true
    }
    
    @objc func tapGestureCallBack() {
        router?.dismissController(controller: self)
    }
}

extension PokemonDetailViewController: PokemonDetailViewControllerProtocol {
    
    func displayResponse(response: PokemonDetailModel.Response) {
        
        UIView.animate(withDuration: timeDurationForAnimation) {
            self.detailViewHeight.constant = self.detailHeight
            self.view.layoutIfNeeded()
        }
        self.pokemonName.text = response.name
        self.weight.text = String(response.weight * 100) + " grams"
        height.text = String(Float(response.height/10)) + " meters"
        interactor?.downloadImage(urlString: response.sprites.front_default)
    }
    
    func stopAnimation() {
        self.activityIndicator.stopAnimating()
    }
    
    func displayImageData(response: Data) {
        pokemonImage.image = UIImage(data: response)
    }
}
