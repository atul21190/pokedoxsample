//
//  File.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation
import UIKit

class PokemonDetailConfigurator {
    
    static func configureVIP(viewController: PokemonDetailViewController) {
        let presenter = PokemonDetailPresenter()
        let interactor = PokemonDetailInteractor()
        let router = PokemonDetailRouter()
        viewController.interactor = interactor
        viewController.router = router
        presenter.viewController = viewController
        interactor.presenter = presenter
    }
}
