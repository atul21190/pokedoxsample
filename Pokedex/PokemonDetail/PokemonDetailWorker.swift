//
//  PokemonDetailWorker.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation

protocol PokemonDetailWorkerProtocol {
    func getPokemonDetail(urlString: String,reponseObject: @escaping (Result<PokemonDetailModel.Response,NetworkError>) -> Void)
    func downloadImage(urlString: String, response: @escaping (Result<Data,NetworkError>) -> Void)
}

class PokemonDetailWorker: PokemonDetailWorkerProtocol {
    
    var service: PokemonDetailServiceProtocol!
    
    init(_ _service: PokemonDetailServiceProtocol) {
        service = _service
    }
    
    func getPokemonDetail(urlString: String,reponseObject: @escaping (Result<PokemonDetailModel.Response,NetworkError>) -> Void)  {
        service.getPokemonDetail(urlString: urlString, result: {
            result in
            switch result {
            case .success(let data):
                if let model = try? JSONDecoder().decode(PokemonDetailModel.Response.self, from: data) {
                    reponseObject(.success(model))
                }
            case .failure(let error):
                reponseObject(.failure(error))
            }
        })
    }
    
    func downloadImage(urlString: String, response: @escaping (Result<Data,NetworkError>) -> Void) {
        service.downloadImage(urlString: urlString) {
            result in
            switch result {
            case .success(let data):
                response(.success(data))
            case .failure(let error):
                response(.failure(error))
            }
        }
    }
}
