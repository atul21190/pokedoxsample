//
//  PokemonDetailPresenter.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation

protocol PokemonDetailPresenterProtocol {
    func presentResponse(response: PokemonDetailModel.Response)
    func presentImageData(response: Data)
    func stopAnimation()
}

class PokemonDetailPresenter {
    var viewController: PokemonDetailViewControllerProtocol?
}

extension PokemonDetailPresenter: PokemonDetailPresenterProtocol {
    
    func presentResponse(response: PokemonDetailModel.Response) {
        DispatchQueue.main.async {
            self.viewController?.displayResponse(response: response)
        }
    }
    
    func presentImageData(response: Data) {
        DispatchQueue.main.async {
            self.viewController?.displayImageData(response: response)
        }
    }
    
    func stopAnimation() {
        DispatchQueue.main.async {
            self.viewController?.stopAnimation()
        }
    }
}
