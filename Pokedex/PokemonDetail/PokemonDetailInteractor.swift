//
//  PokemonDetailInteractor.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation

class PokemonDetailInteractor {
    
    var presenter: PokemonDetailPresenterProtocol?
    let worker: PokemonDetailWorkerProtocol = PokemonDetailWorker(PokemonDetailService())
    
    func getPokemonDetail(urlString: String) {
        worker.getPokemonDetail(urlString: urlString, reponseObject: { result in
            self.presenter?.stopAnimation()
            
            switch result {
            case .success(let response) :
                
                self.presenter?.presentResponse(response: response)
            case .failure(_) :
                break
            }
        }
        )
    }
    
    func downloadImage(urlString: String) {
        worker.downloadImage(urlString: urlString) { result in
            self.presenter?.stopAnimation()
            
            switch result {
            case .success(let response) :
                self.presenter?.presentImageData(response: response)
            case .failure(_) :
                break
            }
        }
    }
    
}
