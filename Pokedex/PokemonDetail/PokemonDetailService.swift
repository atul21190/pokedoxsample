//
//  PokemonDetailService.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation

protocol PokemonDetailServiceProtocol {
    func getPokemonDetail(urlString: String, result: @escaping (Result<Data,NetworkError>) -> Void)
    func downloadImage(urlString: String, result: @escaping (Result<Data,NetworkError>) -> Void)
}

class PokemonDetailService: PokemonDetailServiceProtocol {
    
    func getPokemonDetail(urlString: String, result: @escaping (Result<Data,NetworkError>) -> Void) {
        let query = [String:String]()
        let endPoint = END_POINT + "/\(getPokemonId(urlString: urlString))/"
        APIManager.requestData(method: .GET, urlQuery: query, endPoint: endPoint, response: {
            (data,urlResponse,error) in
            if error != nil {
                result(.failure(NetworkError.generalError))
            } else {
                if let responseData = data {
                    result(.success(responseData))
                } else {
                    result(.failure(NetworkError.generalError))
                }
            }
        })
    }
    
    func getPokemonId(urlString: String) -> Int {
        var digit = [Character]()
        var count = 0
        for character in urlString.reversed() {
            if character == "/" {
                if count == 0 {
                    count += 1
                } else {
                    var value = 0
                    for integer in digit.reversed() {
                        value = value*10 + (integer.hexDigitValue ?? 0)
                    }
                    return value
                }
            } else {
                digit.append(character)
            }
        }
        return 0
    }
    
    func downloadImage(urlString: String, result: @escaping (Result<Data,NetworkError>) -> Void) {
        APIManager.downloadImage(urlString: urlString) {
            (data,urlResponse,error) in
            if error != nil {
                result(.failure(NetworkError.generalError))
            } else {
                if let responseData = data {
                    result(.success(responseData))
                } else {
                    result(.failure(NetworkError.generalError))
                }
            }
        }
    }
}
