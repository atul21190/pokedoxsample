//
//  PokemonDetailModel.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation

class PokemonDetailModel {
    
    struct Request {}
    
    struct Response: Codable {
        let id: Int
        let name: String
        let height: Int
        let weight: Int
        let sprites: Sprite
    }
}

struct Sprite: Codable {
    let front_default: String
}
