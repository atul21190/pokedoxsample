//
//  PokedexInteractor.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation


class PokedexInteractor {
    
    var presenter: PokedexPresenterProtocol?
    var worker: PokedexWorkerProtocol = PokedexWorker(PokedexService())
    
    convenience init(_ _worker: PokedexWorkerProtocol) {
        self.init()
        worker = _worker
    }
    
    func getPokemonList(_ pageOffset: Int) {
        worker.getPokemonList(pageOfset: pageOffset,reponseObject: {result in
            switch result {
            case .success(let response) :
                self.presenter?.presentResponse(response: response)
            case .failure(_) :
                break
            }
        }
        )
    }
}
