//
//  PokedexPresenter.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation


protocol PokedexPresenterProtocol {
    func presentResponse(response: PokedexModel.Response)
}

class PokedexPresenter {
    
    var viewController: PokedexViewProtocol?
    
}

extension PokedexPresenter: PokedexPresenterProtocol {
    
    func presentResponse(response: PokedexModel.Response) {
        DispatchQueue.main.async {
            self.viewController?.displayResponse(response: response)
        }
    }
    
}
