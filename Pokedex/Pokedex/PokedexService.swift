//
//  PokedexService.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation

protocol PokedexServiceProtocol {
    func getPokemonList(pageOffset:Int, result: @escaping (Result<Data,NetworkError>) -> Void)
}

class PokedexService: PokedexServiceProtocol {
    
    func getPokemonList(pageOffset:Int, result: @escaping (Result<Data,NetworkError>) -> Void) {
        let query = ["limit": PAGE_LIMIT,"offset": String(pageOffset)]
        APIManager.requestData(method: .GET, urlQuery: query, endPoint: END_POINT, response: {
            (data,urlResponse,error) in
            if error != nil {
                result(.failure(NetworkError.generalError))
            } else {
                if let responseData = data {
                    result(.success(responseData))
                } else {
                    result(.failure(NetworkError.generalError))
                }
            }
        })
    }
}
