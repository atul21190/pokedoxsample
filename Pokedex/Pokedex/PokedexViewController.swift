//
//  ViewController.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import UIKit


protocol PokedexViewProtocol {
    func displayResponse(response: PokedexModel.Response)
}

class PokedexViewController: UIViewController {
    
    @IBOutlet weak var pokedexTableView: UITableView!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    
    var router: PokedexRouter?
    var interactor: PokedexInteractor?
    var pokemonList = [Pokemon]()
    var pageOffset = 0
    let tableHeaderHeight:CGFloat = 60
    let headerTitle = "Pokedex"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        #if DEBUG
        if let _ = getenv("runningTests") {
            return
        }
        #endif
        PokedexConfigurator.configureVIP(viewController: self)
        setUpView()
        interactor?.getPokemonList(pageOffset)
    }
    
    func setUpView() {
        self.title = ""
        let header = UILabel(frame: CGRect(x: 20, y: 0, width: pokedexTableView.frame.width, height: tableHeaderHeight))
        header.accessibilityIdentifier = "header"
        header.font = UIFont.boldSystemFont(ofSize: 20)
        header.text = headerTitle
        pokedexTableView.tableHeaderView = header
    }
}

extension PokedexViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonList.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokedexCellIdentifier", for: indexPath) as! PokedexCell
        cell.accessibilityIdentifier = "cell_" + String(indexPath.row)
        cell.pokemonName.text = ""
        if indexPath.row == pokemonList.count  {
            pageOffset += 20
            cell.activityIndicator.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
                self.interactor?.getPokemonList(self.pageOffset)
            })
        } else {
            let object = pokemonList[indexPath.row]
            cell.bindData(object.name)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router?.navigateToPokemonDetailScreen(id: pokemonList[indexPath.row].url, viewController: self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > tableHeaderHeight {
            self.title = headerTitle
        } else {
            self.title = ""
        }
    }
}

extension PokedexViewController: PokedexViewProtocol {
    
    func displayResponse(response: PokedexModel.Response) {
        pokemonList.append(contentsOf: response.results)
        self.pokedexTableView.reloadData()
    }
}

