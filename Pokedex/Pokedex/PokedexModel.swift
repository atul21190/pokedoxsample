//
//  PokedexModel.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation

struct PokedexModel {
    
    struct Request {}
    
    struct Response: Codable {
        let results: [Pokemon]
    }
}

struct Pokemon: Codable {
    let name: String
    let url: String
}
