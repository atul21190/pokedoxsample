//
//  PokedexConfigurator.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation
import UIKit

class PokedexConfigurator {
    
    static func configureVIP(viewController: PokedexViewController) {
        let presenter = PokedexPresenter()
        let interactor = PokedexInteractor()
        let router = PokedexRouter()
        viewController.interactor = interactor
        viewController.router = router
        presenter.viewController = viewController
        interactor.presenter = presenter
    }
}
