//
//  PokedexRouter.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation
import UIKit

class PokedexRouter {
    
    func navigateToPokemonDetailScreen(id: String, viewController: UIViewController) {
        let detailScreen = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PokemonDetailViewController") as! PokemonDetailViewController
        detailScreen.urlString = id
        detailScreen.modalPresentationStyle = .overFullScreen
        viewController.navigationController?.present(detailScreen, animated: true, completion: nil)
    }
}
