//
//  PokedexCell.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation
import UIKit

class PokedexCell: UITableViewCell {
    
    @IBOutlet weak var pokemonName: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!

    func bindData(_ name: String) {
        pokemonName.text = name
    }
}
