//
//  PokedexWorker.swift
//  Pokedex
//
//  Created by Atul Kumar on 6/5/2563 BE.
//  Copyright © 2563 Atul Kumar. All rights reserved.
//

import Foundation

protocol PokedexWorkerProtocol {
    func getPokemonList(pageOfset: Int,reponseObject: @escaping (Result<PokedexModel.Response,NetworkError>) -> Void)
}

class PokedexWorker: PokedexWorkerProtocol {
    
    var service: PokedexServiceProtocol!
    
    init(_ _service: PokedexServiceProtocol) {
        service = _service
    }
    
    func getPokemonList(pageOfset: Int,reponseObject: @escaping (Result<PokedexModel.Response,NetworkError>) -> Void)  {
        service.getPokemonList(pageOffset: pageOfset, result: {
            result in
            switch result {
            case .success(let data):
                if let model = try? JSONDecoder().decode(PokedexModel.Response.self, from: data) {
                    reponseObject(.success(model))
                }
            case .failure(let error):
                reponseObject(.failure(error))
            }
        })
    }
}
